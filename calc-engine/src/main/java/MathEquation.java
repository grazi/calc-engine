/**
 * Created by grazi on 19.04.16.
 */
public class MathEquation {

    private double leftVal;
    private double rightVal;
    private char opCode = 'a';
    private double result;


    public MathEquation() {
    }

    public MathEquation(char opCode, double leftVal, double rightVal) {
        this.opCode = opCode;
        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }

    // Überladen
    public void execute (double leftVal, double rightVal) {
        this.leftVal = leftVal;
        this.rightVal = rightVal;
        execute();
    }

    // Überladen -> Methode heisst gleich, sie besitzt aber andere Übergabeparameter
    public void execute (int leftVal, int rightVal) {
        this.leftVal = leftVal;
        this.rightVal = rightVal;
        execute();
    }

    public void execute () {

        /*  Variante mit ifElse Konstrukt
        if (opCode == 'a') {
            result = leftVal + rightVal;
        } else if (opCode == 's') {
            result = leftVal - rightVal;
        } else if (opCode == 'd') {
            if (rightVal != 0.0) {
                result = leftVal / rightVal;
            } else {
                result = 0.0;
            }
        } else if (opCode == 'm') {
            result = leftVal * rightVal;
        } else {
            System.out.println("Error - invalid opCode");
            result = 0.0;
        }
        */

        // Variante mit Switch Stmt.
        switch (opCode) {
            case 'a':
                result = leftVal + rightVal;
                break;
            case 's':
                result = leftVal - rightVal;
                break;
            case 'd':
                // Verkürzte Var.
                result = rightVal != 0.0 ? leftVal / rightVal : 0.0;
                break;
            case 'm':
                result = leftVal * rightVal;
                break;
            default:
                System.out.println("Error - invalid opCode");
                result = 0.0;
                break;
        }
    }

    public double getResult() {
        return result;
    }


    public double getLeftVal() {
        return leftVal;
    }

    public void setLeftVal(double leftVal) {
        this.leftVal = leftVal;
    }

    public double getRightVal() {
        return rightVal;
    }

    public void setRightVal(double rightVal) {
        this.rightVal = rightVal;
    }

    public char getOpCode() {
        return opCode;
    }

    public void setOpCode(char opCode) {
        this.opCode = opCode;
    }


}
