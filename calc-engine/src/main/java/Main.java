/**
 * Created by grazi on 19.04.16.
 */
public class Main {
    public static void main(String[] args) {

        MathEquation [] equations = new MathEquation[4];
        equations[0] = new MathEquation('d', 100.0, 50.0);
        equations[1] = new MathEquation('a', 25.0, 92.0);
        equations[2] = new MathEquation('s', 225.0, 17.0);
        equations[3] = new MathEquation('m', 11.0, 3.0);

        for (MathEquation equatation : equations) {
            equatation.execute();
            System.out.print("Result = ");
            System.out.println(equatation.getResult());
        }

    }
}
